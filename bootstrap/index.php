<?php
/**
 * rodrigopa
 */
require '../vendor/autoload.php';
session_start();

// creating ioc and registering coservices
$builder = new \DI\ContainerBuilder();
$builder->addDefinitions('../config/di.php');
$container = $builder->build();

//
$router = $container->get('router');

// Fetch method and URI from somewhere
$httpMethod = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER['REQUEST_URI'];

// Strip query string (?foo=bar) and decode URI
if (false !== $pos = strpos($uri, '?')) {
  $uri = substr($uri, 0, $pos);
}
$uri = rawurldecode($uri);

$routeInfo = $router->dispatch($httpMethod, $uri);

switch ($routeInfo[0]) {
  case FastRoute\Dispatcher::FOUND:
    list($controllerClass, $method) = $routeInfo[1];
    $container->get($controllerClass)->{$method}();
    exit;
  case FastRoute\Dispatcher::NOT_FOUND:
  case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
    readfile('../resources/views/common/404.html');
    exit;
}
