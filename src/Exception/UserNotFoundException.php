<?php

namespace App\Exception;

class UserNotFoundException extends \Exception
{
    protected $message = 'Usuário não encontrado.';
}
