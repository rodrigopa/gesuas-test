<?php

if (!function_exists('flash_error_message')) {
  function flash_error_message() {
    $message = $_SESSION['flash.error_message'] ?? null;
    unset($_SESSION['flash.error_message']);
    return $message;
  }
}

if (!function_exists('flash_success_message')) {
    function flash_success_message() {
        $message = $_SESSION['flash.success_message'] ?? null;
        unset($_SESSION['flash.success_message']);
        return $message;
    }
}

