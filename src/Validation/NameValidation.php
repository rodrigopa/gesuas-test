<?php

namespace App\Validation;

use App\Exception\ValidationException;

class NameValidation
{
  private static function sanitize(array $items, string $key)
  {
    return isset($items[$key]) ? trim($items[$key]) : null;
  }

  public static function validate(array $items)
  {
    $name = self::sanitize($items, 'name');
    if (!$name) throw new ValidationException('Name is required.');
    return ['name' => $name];
  }
}
