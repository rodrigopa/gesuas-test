<?php

namespace App\Contract;

interface NISGenerator {
  function generate(): string;
}
