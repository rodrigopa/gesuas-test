<?php

namespace App\Contract;

use App\Persistence\User;

interface UserRepositoryInterface {
  function insert(User $user): void;
  function searchByNIS(string $nis): User;
}
