<?php

namespace App\Persistence;

class User
{
  public function __construct(public string $name, public string $nis)
  {
  }

  public function toArray()
  {
      return [
          'name' => $this->name,
          'nis' => $this->nis,
      ];
  }
}
