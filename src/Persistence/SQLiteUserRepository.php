<?php

namespace App\Persistence;

use App\Contract\UserRepositoryInterface;
use App\Exception\UserNotFoundException;
use Medoo\Medoo;

class SQLiteUserRepository implements UserRepositoryInterface
{
    protected Medoo $connection;

    public function __construct()
    {
        $this->connection = new Medoo([
            'type' => 'sqlite',
            'database' => __DIR__ . '/../../db.sqlite',
            'logging' => true,
        ]);
    }

    function insert(User $user): void
    {
        $this->connection->insert('users', $user->toArray());
    }

    function searchByNIS(string $nis): User
    {
        $user = $this->connection->get('users', ['nis', 'name'], ['nis' => $nis]);

        if (!$user) {
            throw new UserNotFoundException();
        }

        return new User($user['name'], $user['nis']);
    }
}
