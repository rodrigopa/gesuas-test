<?php

namespace App\Service;

use App\Contract\NISGenerator;

class RandomNISGenerator implements NISGenerator
{
    function generate(): string
    {
      return str_pad(mt_rand(1, str_repeat(9, 11)), 11, '0', STR_PAD_LEFT);
    }
}
