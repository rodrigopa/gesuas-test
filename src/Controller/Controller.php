<?php

namespace App\Controller;

use App\Contract\NISGenerator;
use App\Validation\NameValidation;

class Controller
{
  protected function getBackURL()
  {
    $referer = $_SERVER['HTTP_REFERER'] ?? '';
    return preg_match('/^http:\/\/localhost:8000/i', $referer) !== false
      ? $referer
      : 'http://localhost:8000';
  }


  protected function redirectWithError(string $message)
  {
    $_SESSION['flash.error_message'] = $message;
    header('Location: ' . $this->getBackURL());
  }

    protected function redirectWithSuccess(string $message)
    {
        $_SESSION['flash.success_message'] = $message;
        header('Location: ' . $this->getBackURL());
    }
}
