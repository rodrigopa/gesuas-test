<?php

namespace App\Controller;

use App\Contract\NISGenerator;
use App\Contract\UserRepositoryInterface;
use App\Persistence\User;
use App\Validation\NameValidation;
use App\Exception\ValidationException;

class UserController extends Controller
{
    public function __construct(
        protected NISGenerator $generator,
        protected UserRepositoryInterface $userRepository,
    ) {
    }

    public function index()
    {
        $nis = $_GET['nis'] ?? null;

        try {
            if (!is_null($nis)) {
                $user = $this->userRepository->searchByNIS($nis);
                $_SESSION['flash.success_message'] = "Nome: {$user->name}, NIS: {$user->nis}";
            }
        } catch (\Exception $e) {
            $_SESSION['flash.error_message'] = $e->getMessage();
        }

        require_once __DIR__ . '/../../resources/views/users/index.php';
    }

    public function create()
    {
        require_once __DIR__ . '/../../resources/views/users/form.php';
    }

    public function store()
    {
        try {
            list('name' => $name) = NameValidation::validate($_POST);
            $nis = $this->generator->generate();
            $this->userRepository->insert(new User($name, $nis));
            $this->redirectWithSuccess("Usuário inserido, NIS: {$nis}.");
        } catch (\Exception $e) {
            $this->redirectWithError($e->getMessage());
        }
    }
}
