O projeto utiliza features do php >= 8.1.

Caso possua na máquina esta versão, na raiz do projeto, rode no seu terminal:

    php -S localhost:8000 -t ./bootstrap/

Caso prefira usar o docker, está configurado o Docker Compose para rodar a aplicação.

    docker compose up

Abra http://localhost:8000 no seu navegador.

Tecnologias utilizadas:
- FastRoute como dispatcher de rotas;
- Medoo como framework de persistência de dados;
- PHP-DI como container para injeção de dependências.
- PHPStorm como IDE

Buscar usuários
- /users

Inserir usuários
- /users/create