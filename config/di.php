<?php

use App\Contract\NISGenerator;
use App\Contract\UserRepositoryInterface;
use App\Persistence\SQLiteUserRepository;
use App\Service\RandomNISGenerator;

return [
    'router' => fn () => FastRoute\simpleDispatcher(require '../routes/routes.php'),
    NISGenerator::class => DI\create(RandomNISGenerator::class),
    UserRepositoryInterface::class => DI\create(SQLiteUserRepository::class),
];
