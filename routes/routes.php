<?php

use App\Controller\UserController;

return function (FastRoute\RouteCollector $router) {
  $router->addRoute('GET', '/users/create', [UserController::class, 'create']);
  $router->addRoute('POST', '/users', [UserController::class, 'store']);
  $router->addRoute('GET', '/users', [UserController::class, 'index']);
};