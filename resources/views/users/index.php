<!doctype html>

<html lang="pt-BR">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Users</title>

  <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/milligram/1.4.1/milligram.css">
  <style>
    html, body {
      display: flex;
      width: 100%;
      height: 100%;
      align-items: center;
      justify-content: center;
    }
    .alert {
      background: #d1d1d1;
      border-radius: 0.4rem;
      display: block;
      font-size: 1rem;
      font-weight: 600;
      height: 3rem;
      letter-spacing: .1rem;
      line-height: 3rem;
      margin-bottom: 2.5rem;
      text-align: center;
      text-transform: uppercase;
    }
    .alert-error {
      background-color: red;
      color: white;
    }
    .alert-success {
        background-color: #b5f0b5;
        color: white;
    }
  </style>
</head>

<body>
  <div>
    <h2 style="display: block">Busca por NIS</h2>
    <?php
        $message = flash_error_message();
        if (!!$message):
    ?>
        <p class="alert alert-error"><?php echo $message;?></p>
    <?php endif; ?>
    <?php
        $message = flash_success_message();
        if (!!$message):
    ?>
        <p class="alert alert-success"><?php echo $message;?></p>
    <?php endif; ?>
    <form method="get" action="/users">
      <label for="nis">NIS</label>
      <input type="tel" id="nis" name="nis" maxlength="11" />
      <button>Buscar</button>
      <a href="/users" class="button">Limpar</a>
    </form>
  </div>
</body>
</html>